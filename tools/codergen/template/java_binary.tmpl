{{/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */}}


{{define "Java.Encoder"}}
  @Override¶
  public void encode(@NotNull Encoder e, BinaryObject obj) throws IOException {»¶
    {{File.ClassName .}} o = ({{File.ClassName .}})obj;¶
    {{range .Fields}}
      {{Call "Java.Encode" (Var .Type "o." (File.FieldName .Name))}}¶
    {{end}}
  «}¶
{{end}}

{{define "Java.Encode.Primitive"}}e.{{Lower .Type.Method}}({{.Name}});{{end}}
{{define "Java.Encode.Struct"}}e.value({{.Name}});{{end}}
{{define "Java.Encode.Pointer"}}e.object({{.Name}});{{end}}
{{define "Java.Encode#binary.Object"}}e.object({{.Name}});{{end}}
{{define "Java.Encode.Interface"}}e.object({{.Name}}.unwrap());{{end}}
{{define "Java.Encode.Any"}}e.object({{.Name}});{{end}}

{{define "Java.Encode.Slice"}}
  e.int32({{.Name}}.length);¶
  for (int i = 0; i < {{.Name}}.length; i++) {»¶
    {{Call "Java.Encode" (Var .Type.ValueType .Name "[i]")}}¶
  «}
{{end}}

{{define "Java.Encode.Array"}}
  for (int i = 0; i < {{.Type.Size}}; i++) {»¶
    {{Call "Java.Encode" (Var .Type.ValueType .Name "[i]")}}¶
  «}
{{end}}

{{define "Java.Encode.Map"}}throw new RuntimeException("Java map handling not implemented");{{end}}

{{define "Java.Decoder"}}
  @Override¶
  public void decode(@NotNull Decoder d, BinaryObject obj) throws IOException {»¶
    {{File.ClassName .}} o = ({{File.ClassName .}})obj;¶
    {{range .Fields}}
      {{Call "Java.Decode" (Var .Type "o." (File.FieldName .Name))}}¶
    {{end}}
  «}¶
{{end}}

{{define "Java.Decode.Primitive"}}{{.Name}} = d.{{Lower .Type.Method}}();{{end}}
{{define "Java.Decode.Struct"}}{{.Name}} = new {{File.ClassName .Type.Name}}(d);{{end}}
{{define "Java.Decode.Pointer"}}{{.Name}} = ({{Call "Java.Type" .Type}})d.object();{{end}}
{{define "Java.Decode#binary.Object"}}{{.Name}} = d.object();{{end}}
{{define "Java.Decode.Interface"}}{{.Name}} = {{Call "Java.Type" .Type}}.wrap(d.object());{{end}}
{{define "Java.Decode.Any"}}{{.Name}} = (Box)d.object();{{end}}

{{define "Java.Decode.Slice"}}
  {{.Name}} = new {{Call "Java.Type" .Type.ValueType}}[d.int32()];¶
  for (int i = 0; i <{{.Name}}.length; i++) {»¶
    {{Call "Java.Decode" (Var .Type.ValueType .Name "[i]")}}¶
  «}
{{end}}

{{define "Java.Decode.Array"}}
  {{.Name}} = new {{Call "Java.Type" .Type}}[{{.Type.Size}}];¶
  for (int i = 0; i < {{.Type.Size}}; i++) {»¶
    {{Call "Java.Decode" (Var .Type.ValueType .Name "[i]")}}¶
  «}
{{end}}

{{define "Java.Decode.Map"}}throw new RuntimeException("Java map handling not implemented");{{end}}

{{define "Java.Field"}}
  {{Call "Java.Type" .Type}} {{File.FieldName .Name}};¶
{{end}}

{{define "Java.Accessors"}}
  ¶
  public {{Call "Java.Type" .Type}} {{File.Getter .Name}}() {»¶
    return {{File.FieldName .Name}};¶
  «}¶
  ¶
  public void {{File.Setter .Name}}({{Call "Java.Type" .Type}} v) {»¶
    {{File.FieldName .Name}} = v;¶
  «}¶
{{end}}

{{define "Java.ClassBody"}}
  ¶{{/*Newline after section marker*/}}
  {{range .Struct.Fields}}{{template "Java.Field" .}}{{end}}
  ¶
  // Constructs a default-initialized {@link {{File.ClassName .Struct}}}.¶
  public {{File.ClassName .Struct}}() {»¶
  «}¶
  ¶
  // Constructs and decodes a {@link {{File.ClassName .Struct}}} from the {@link Decoder} d.¶
  public {{File.ClassName .Struct}}(Decoder d) throws IOException {»¶
    Klass.INSTANCE.decode(d, this);¶
  «}¶
  {{range .Struct.Fields}}{{template "Java.Accessors" .}}{{end}}
  ¶
  @Override @NotNull¶
  public BinaryClass klass() { return Klass.INSTANCE; }¶
  ¶
  public static byte[] IDBytes = {
    {{range .Struct.ID}}{{ToS8 .}}, {{end}}
  •};¶
  public static BinaryID ID = new BinaryID(IDBytes);¶
  ¶
  static {»¶
    Namespace.register(ID, Klass.INSTANCE);¶
  «}¶
  //{{/*Comment the following section marker*/}}
{{end}}

{{define "Java.KlassBody"}}
  ¶{{/*Newline after section marker*/}}
  INSTANCE;¶
  ¶
  @Override @NotNull¶
  public BinaryID id() { return ID; }¶
  ¶
  @Override @NotNull¶
  public BinaryObject create() { return new {{File.ClassName .Struct}}(); }¶
  ¶
  {{template "Java.Encoder" .Struct}}
  ¶
  {{template "Java.Decoder" .Struct}}
  //{{/*Comment the following section marker*/}}
{{end}}

{{define "Java.File"}}
  §{{$.Copyright}}§¶
  package {{.JavaPackage}};¶
  ¶
  import org.jetbrains.annotations.NotNull;¶
  ¶
  import com.android.tools.rpclib.binary.BinaryClass;¶
  import com.android.tools.rpclib.binary.BinaryID;¶
  import com.android.tools.rpclib.binary.BinaryObject;¶
  import com.android.tools.rpclib.binary.Decoder;¶
  import com.android.tools.rpclib.binary.Encoder;¶
  import com.android.tools.rpclib.binary.Namespace;¶
  {{range .Struct.Fields}}{{Call "Java.Import" .Type}}{{end}}
  ¶
  import java.io.IOException;¶
  ¶
  {{if .Struct.Exported}}public•{{end}}
  final class {{File.ClassName .Struct}}{{if $e := .Struct.Tags.Get  "implements"}} extends {{File.ClassName $e}}{{end}} implements BinaryObject {»¶
    //{{Section "Java.ClassBody"}}¶
    public enum Klass implements BinaryClass {»¶
      //{{Section "Java.KlassBody"}}¶
    «}¶
  «}¶
{{end}}
